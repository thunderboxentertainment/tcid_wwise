State Group	ID	Name			Wwise Object Path	Notes
	1523462916	Turn_Phase			\Soundtrack\Turn_Phase	
	3211383847	Game_Phase			\Soundtrack\Game_Phase	
	3795238602	TCID_Phase_States			\Import States\TCID_Phase_States	

State	ID	Name	State Group			Notes
	0	None	Turn_Phase			
	209137191	Action	Turn_Phase			
	1786804762	Damage	Turn_Phase			
	1837356725	Override	Turn_Phase			
	4030710154	Planning	Turn_Phase			
	4215451718	Relief	Turn_Phase			
	0	None	Game_Phase			
	1125500713	Intro	Game_Phase			
	1593864692	Defeat	Game_Phase			
	2220117742	Yellow_Alert	Game_Phase			
	2716678721	Victory	Game_Phase			
	3697663636	Orange_Alert	Game_Phase			
	3945144913	Initial_Damage	Game_Phase			
	4038346005	Red_Alert	Game_Phase			
	0	None	TCID_Phase_States			
	1256202792	PLAN	TCID_Phase_States			
	1368510894	SCN_TRIGGER	TCID_Phase_States			
	1926883979	SCENA	TCID_Phase_States			
	2565691689	SCN_OVRR	TCID_Phase_States			
	3011204530	MOVE	TCID_Phase_States			

Game Parameter	ID	Name			Wwise Object Path	Notes
	1006694123	Music_Volume			\Settings\Music_Volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	1506190228	Computer Bus			\Default Work Unit\Master Audio Bus\Computer Bus	
	1604779591	Effects Bus			\Default Work Unit\Master Audio Bus\Effects Bus	
	2018813558	VO Bus			\Default Work Unit\Master Audio Bus\VO Bus	
	3127962312	Music Bus			\Default Work Unit\Master Audio Bus\Music Bus	
	3600729941	UI Bus			\Default Work Unit\Master Audio Bus\UI Bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Effect plug-ins	ID	Name	Type				Notes
	3100619589	Starship_Matrix	Wwise Matrix Reverb			
	4169379491	Starship	Wwise RoomVerb			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

